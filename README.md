# SmartStore - Prueba Técnica Between

Prueba técnica para el puesto de desarrollador Senior en Between

## Introducción

En este readme explico brevemente el funcionamiento de la app, la manera de levantarla y las deciones de diseño.

La app está creada con create-react-app. Tengo más experiencia trabajando con monorepos, sobre todo con https://nx.dev/ . En este caso, debido al requisito de los scripts y que se pedía no usar typescript, he descartado esta opción por la complejidad añadida que supone.

## Repositorio.

El código de la aplicación se encuentra alojado en las siguientes urls:

- **Repositorio**: https://gitlab.com/PabloGT/between-mobile-store
- **Comits**: https://gitlab.com/PabloGT/between-mobile-store/-/commits/main
- **Gráfico Ramas**: https://gitlab.com/PabloGT/between-mobile-store/-/network/main
  
En la redacción de la prueba existía un requisito que consistía en entregar el código de manera evolutiva. Para ello he supuesto que se entrega el proyecto con 2 funcionalidades:
1. La primera de ellas sería el catálogo de productos sin la opción de detalle del producto y la posibilidad de comprarlo.
2. La segunda entrega sería la aplicación completa, ya con el detalle del producto y el formulario para comparlo.

En todo momento se ha cuidado que en cualquiera de los commits, tanto de la rama principal como de cualquiera de las ramas de funcionalidades, la aplicación funcione y tanto los tests como el linter pasen sin errores.

Para hacer los commits se ha seguido la especificación de los conventional commits (https://www.conventionalcommits.org/en/v1.0.0/)

Se ha creado un fichero de configuración para la integración continua en gitlab, que levanta una pipe con el lint y el test en cada commit a main. El despliegue se hace automáticamente a netlify igualmente en el commit a main (Esto se gestiona desde netlify).

## Inicialización de la app

### Producción
La aplicación está disponible en producción, es decir, para consultar sin necesidad de levantarla en local en la siguiente url:

https://beetween-smartstore.netlify.app

### Local
Para arrancar la app, una vez instaladas las dependencias con npm install, antes de lanzar el comando _npm start_ necesario para levantar el servidor de desarrollo, necesitamos crear un fichero '.env.local' con las variables de entorno siguientes:
- REACT_APP_API_URL=https://front-test-api.herokuapp.com/

Los comandos existentes son los pedidos en la prueba, con el añadido del comando lint-fix porque tenía dudas de si en el comando lint se requería el fix (aunque si utilizamos el lint como parte de la integración continua, no deberíamos hacer que ese comando modifique nada del código)

### Diseño

Tal y como decía la prueba, el diseño es libre. He descartado el uso de un framework CSS en un principio para evitar complicaciones, aunque creo que al final fué una mala decisión, seguramente por muy sencilla que parezca la prueba, seguramente un framework css me hubiese ayudado. De elegir alguno, optaría por Bootstrap 5 (Por una cuestión de experiencia con bootstrap, llevo trabajando con él desde la versión 3).

También he optado por usar ficheros css por la facilidad en la prueba. En cuando a estilos, tengo experiencia con styled components, pero lo he descartado para la prueba para no complicarme con demasiadas dependencias.

### Estructura

Para estructurar la aplicación he optado por hacer una estructura parecida a la que utilizaba en mi último proyecto y que nos ha dado muy buenos resultados. Este proyecto se gestionaba con un monorepo nx, que tenia una sección de apps y otra de libs. En este caso, la aplicación está estructura en 2 apartados muy diferenciados:

- **Módulos**: Se corresponde con el apartado funcional. Cada funcionalidad iría en un módulo aparte. En ese caso, solo tenemos una funcionalidad, que es el ecommerce. En caso de necesitar por ejemplo un backoffice para gestionar los productos, esta estructura nos serviría, simplemente tendríamos que añadir el módulo de backoffice
- **Packages**: En este apartado están los paquetes de componentes a utilizar. Estos componentes se ordenan por entidad, de manera que funcionan como librerías de componentes reutilizables. Dentro de los paquetes tambien está la sección core. En mi último proyecto, el core eran librerías separadas, completamente agnósticas al framework quen os permitían compartír código en el proyecto legacy y en el nuevo con el beneficio que nos aporta el ir madurando esas librerías en producción. Está estructurado siguiendo las recomendaciones de la arquitectura hexagonal.

*Programar este proyecto con un core tan complejo no es recomendable, pero me pareció buena idea mostrarlo. Ha sido un éxito su implementación en mi último proyecto y por eso he decidido hacerlo asi*

En cuando a los componetes los he clasificado en 3 tipos:
- **Components**: Son los componentes puramente presentacionales. Se limitan a recibir entidades de dominio por props, presentarlas en pantalla y devolver entidades de dominio a traves de eventos.
- **Containers**: Son los encargados de obtener los datos de servicio externos. Siempre se hace a través de un customHook para desacoplar el detalle de implementación del servicio de obtención de datos de la capa de ui, para poder tener más libertad y poder cambiar el gestor de estado de la aplicación sin afectar a todos los componentes. Estos containers recibirán como parámetros los identificadores de las entidades para poder acceder a los datos.
- **Pages**: Su responsabilidad es únicamente la de componer los components y containers y obtener los datos de la url. De esta forma, desacoplamos los containers de la url y podremos utilizar un container de manera aislada siempre y cuando tengamos los ids que necesite ese container, bien estén en la url o vengan informados como como parte de la respuesta de otro servicio.

Para enviar los componentes de acciones tanto a la lista (link al detalle), como al header (cesta de la compra), como el formulario para elegir el producto he usado la composición de componentes. Simplemente se define una interfaz de las props que deben implementar los componentes dinámicos (esto se hace más fácil con typescript) y se pasa ese componente como parámetro. Esto nos permite una reutilización de los componentes del apartado de packages, de manera que la misma lista la podemos utilizar para mostrar los productos en la parte de ecommerce o en la parte de backoffice, simplemente en cada apartado enviaremos un componente distinto como parámetro de manera que en cada tabla dirija a la página correspondiente. Lo mismo para el detalle del producto, podremos enviar por parámetro el componente para añadir elementos a la cesta en el módulo de ecommerce y el componente para añadir stock al almacén en el apartado de backoffice.

En el filtro de los productos en el listado, como podréis observar, lo que hace el input es devolver un filtro que luego se aplica al container de lista para filtrar los elementos. Mi idea era que en vez de enviar directamente el filtro desde el input al container, que se actualizase un queryParam en la url, que la página observase ese cambio y que la responsabilidad de pasar el filtro al container recayese en la página. Con esta funcionalidad, podríamos aplicar directamente filtros desde una url.

Para la gestión del estado he utilizado swr. Nos permite cachear las peticiones durante un tiempo determinado, tal y como requiere el enunciado de la prueba, por lo que es suficiente para esta prueba.

### Testing

En la parte de testing he dejado varios tests unitarios, sobre todo en la parte del core. No me dió tiempo a configurar más herramientas y al final he tenido que descartar los test de componentes, hooks y e2e por cuestión de tiempo.

En mi último proyecto teniamos test del core tanto unitario como e2e (en este caso los test e2e se refieren solo al core, es decir, consumíamos el servicio de back sin levantar la app en el browser). Se hacían ambos con jest.

Los componentes, containers y hooks los testeabamos con testing library, teniendo que hacer mock del hook en el container y del servicio del core para probar el hook.

Tengo experiencia en los test e2e con cypress, que se limitaban en mi caso a simplemente probar el enrutado (una especie de user-journey).

Por último mencionar que me hubiese gustado añadir storybook al proyecto, pero por cuestión de tiempo me fué imposible. Tambien lo utilizábamos en mi último proyecto.

## Funcionamiento

En cuando al funcionamiento, creo que están todos los requisitos, pero me he encontrado con un problema a la hora de actualizar la cesta de la compra. No sé si he entendio bien como es el servicio, pero me he encontrado que envíes los productos que envies, el contador nunca sube de 1. Tampoco tenemos un identifcador del carro. De todas formas, entiendo que lo que se necesita es probar como actualizar ese componente al añadir elementos al carro, y eso funciona.