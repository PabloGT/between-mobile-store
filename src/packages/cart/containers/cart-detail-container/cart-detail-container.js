import { useCartDetail } from '../../hooks/use-cart-detail'

import shoppingBag from './shopping_bag.svg'

import './cart-detail-container.css'

export const CartDetailContainer = () => {
  const { cart, loading } = useCartDetail()

  if (loading || !cart) return null

  return (
    <div className="shopping-bag">
      <img src={shoppingBag} className="bag-image" alt="Cesta de la compra" />
      <div className="bag-size">{cart.productInstances.length}</div>
    </div>
  )
}
