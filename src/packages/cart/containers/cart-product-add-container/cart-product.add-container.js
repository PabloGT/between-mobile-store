import { useProductDetail } from '../../../product/hooks/use-product-detail'
import { useCartProductAdd } from '../../hooks/use-cart-product-add'

import { ProductInstanceFormComponent } from '../../../product/components/product-instance-form'

import './cart-product-add-container.css'

export const CartProductAddContainer = ({ productDefinitionId }) => {
  const { cartProductAdd } = useCartProductAdd()
  const { productDefinition, loading } = useProductDetail(productDefinitionId)

  if (loading || !productDefinition) return null

  const handleSubmit = (productInstance) => {
    cartProductAdd(productInstance)
  }

  return (
    <div className="cart-product-add-container">
      <ProductInstanceFormComponent
        productDefinition={productDefinition}
        onSubmit={handleSubmit}
      />
    </div>
  )
}
