import { CartService } from '../core'

import { useCartDetail } from './use-cart-detail'

export const useCartProductAdd = () => {
  const { cart, error, loading, mutate } = useCartDetail()

  const cartProductAdd = async (productInstance) => {
    if (!cart) return
    const modifiedCart = await CartService.productInstanceAdd(
      cart,
      productInstance
    )
    mutate(modifiedCart, { revalidate: false })
  }

  return { cart, error, loading, cartProductAdd }
}
