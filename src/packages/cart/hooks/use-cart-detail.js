import { Cart } from './../core/domain/cart'

import useSwr from 'swr'

export const useCartDetail = () => {
  const { data, error, isValidating, mutate } = useSwr(
    'cart',
    () => new Cart(),
    { revalidateOnFocus: false }
  )

  return { cart: data, error, loading: isValidating, mutate }
}
