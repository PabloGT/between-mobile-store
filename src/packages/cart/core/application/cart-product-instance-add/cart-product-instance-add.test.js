import { cartProductInstanceAdd } from './cart-product-instance-add'

const PRODUCT_INSTANCE_SAMPLE_1 = {
  productDefinitionId: 'ZmGrkLRPXOTpxsU4jjAcv',
  color: {
    code: 1000,
    name: 'Black'
  },
  storage: {
    code: 2000,
    name: '16 GB'
  }
}

const CART_SAMPLE = {
  productInstances: [null, null, null]
}

describe('Cart Product Instance Add Use Case Test', () => {
  it('Calls correcty to repo update method', async () => {
    // Repo Mock
    const repositoryMock = {
      update: jest.fn(() => Promise.resolve())
    }

    // Create use Case
    const cartProductInstanceAddUseCase =
      cartProductInstanceAdd(repositoryMock)

    // Execution
    await cartProductInstanceAddUseCase(
      JSON.parse(JSON.stringify(CART_SAMPLE)),
      PRODUCT_INSTANCE_SAMPLE_1
    )

    // Expectations
    const expected = {
      ...CART_SAMPLE,
      productInstances: [
        ...CART_SAMPLE.productInstances,
        PRODUCT_INSTANCE_SAMPLE_1
      ]
    }

    expect(repositoryMock.update).toBeCalledTimes(1)
    expect(repositoryMock.update).toBeCalledWith(expected)
  })
})
