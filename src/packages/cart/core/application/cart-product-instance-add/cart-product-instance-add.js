export function cartProductInstanceAdd (repo) {
  return async function (cart, productInstance) {
    cart.productInstances.push(productInstance)
    return await repo.update(cart)
  }
}
