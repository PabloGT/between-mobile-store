import { cartAdapterResponse } from './cart-adapter-response'

const SAMPLE_RESPONSE = { count: 5 }

describe('Cart Adapter Response Test', () => {
  it('Sets correctly the products Array', () => {
    const cart = cartAdapterResponse(SAMPLE_RESPONSE)
    expect(cart.productInstances.length).toEqual(5)
  })
})
