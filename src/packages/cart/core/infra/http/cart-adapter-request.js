export const cartAdapterRequest = function (cart) {
  const lastProduct = cart.productInstances.pop()
  const cartRequest = {
    id: lastProduct.productDefinitionId,
    colorCode: lastProduct.color.code,
    storageCode: lastProduct.storage.code
  }
  return cartRequest
}
