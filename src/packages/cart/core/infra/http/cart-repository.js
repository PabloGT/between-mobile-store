import axios from 'axios'

import { cartAdapterRequest } from './cart-adapter-request'
import { cartAdapterResponse } from './cart-adapter-response'

const BASE_URL = 'api/cart'

export class CartRepositoryHttp {
  async update (cart) {
    return (
      axios
        .post(`${BASE_URL}`, cartAdapterRequest(cart))
        .then((res) => cartAdapterResponse(res.data))
    )
  }
}
