import { Cart } from '../../domain/cart'

export const cartAdapterResponse = function (cartResponse) {
  const cart = new Cart()
  return { ...cart, productInstances: new Array(cartResponse.count) }
}
