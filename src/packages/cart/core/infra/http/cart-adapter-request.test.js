import { ProductInstance } from '../../../../product/core/domain/product'
import { cartAdapterRequest } from './cart-adapter-request'

const SAMPLE_PRODUCT_INSTANCE = new ProductInstance(
  1,
  {
    code: 1000,
    name: 'Black'
  },
  {
    code: 2000,
    name: '16 GB'
  }
)

const SAMPLE_CART = {
  productInstances: [null, null, null, null, SAMPLE_PRODUCT_INSTANCE]
}

describe('Cart Adapter Request Test', () => {
  it('Gets correctly the last product added', () => {
    const cartRequest = cartAdapterRequest(SAMPLE_CART)

    const expected = { id: 1, colorCode: 1000, storageCode: 2000 }
    expect(cartRequest).toEqual(expected)
  })
})
