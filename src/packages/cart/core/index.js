import { CartRepositoryHttp } from './infra/http/cart-repository'

import { cartProductInstanceAdd } from './application/cart-product-instance-add'

const repo = new CartRepositoryHttp()

export const CartService = {
  productInstanceAdd: cartProductInstanceAdd(repo)
}
