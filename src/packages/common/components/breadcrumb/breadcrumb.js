import { Link, useLocation } from 'react-router-dom'

import './breadcrumb.css'

export const Breadcrumb = () => {
  const location = useLocation()

  const pathcrumbs = location.pathname
    .split('/')
    .filter((pathcrumb) => pathcrumb)
    .map((pathcrumb, index) => ({
      path:
        index > 0 ? `${pathcrumbs[index - 1]}/${pathcrumb}` : `/${pathcrumb}`,
      caption: pathcrumb
    }))

  return (
    <nav>
      <ol className="breadcrumb">
        <li className="breadcrumb-item">
          <Link to="/">Inicio</Link>
        </li>
        {pathcrumbs.map((pathcrumb) => (
          <li className="breadcrumb-item" key={pathcrumb.path}>
            <Link to={`${pathcrumb.path}`}>{pathcrumb.caption}</Link>
          </li>
        ))}
      </ol>
    </nav>
  )
}
