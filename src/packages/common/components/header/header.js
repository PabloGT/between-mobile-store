import { Link } from 'react-router-dom'

import logo from './../../../../logo.svg'

import './header.css'
import { Breadcrumb } from '../breadcrumb'

export const Header = ({ CustomComponent }) => {
  return (
    <header>
      <Link to="/">
        <img src={logo} id="logo" alt="logo" />
      </Link>
      <div className="right-zone">
        <div className="breadcrumb-container">
          <Breadcrumb />
        </div>
        {CustomComponent && (
          <div className="custom-component-container">
            <CustomComponent />
          </div>
        )}
      </div>
    </header>
  )
}
