import { useProductList } from '../../hooks/use-product-list'

import { ProductDefinitionDetailCardComponent } from '../../components/product-definition-detail-card'

import './product-definition-list.css'

export const ProductDefinitionListContainer = ({
  filter,
  ItemActionsComponent
}) => {
  const { list, loading } = useProductList(filter)

  if (loading) return <span>Cargando....</span>

  return (
    <div className="product-definition-list-container">
      {(list || []).map((productDefinition) => (
        <ProductDefinitionDetailCardComponent
          key={productDefinition.id}
          productDefinition={productDefinition}
          ActionsComponent={ItemActionsComponent}
        />
      ))}
    </div>
  )
}
