import { ProductDefinitionDetailComponent } from '../../components/product-definition-detail/product-definition-detail'
import { useProductDetail } from '../../hooks/use-product-detail'

export const ProductDefinitionDetailContainer = ({
  productDefinitionId,
  ActionsComponent
}) => {
  const { productDefinition, loading } = useProductDetail(productDefinitionId)

  if (loading || !productDefinition) return <div>Cargando...</div>

  // Create a wrapper to make props interface compatible
  const ActionsComponentWrapper = ({ productDefinition }) => {
    if (!ActionsComponent) return null
    return <ActionsComponent productDefinitionId={productDefinition.id} />
  }

  return (
    <ProductDefinitionDetailComponent
      productDefinition={productDefinition}
      ActionsComponent={ActionsComponentWrapper}
    />
  )
}
