import { ProductDefinitionRepositoryHttp } from './infra/http/product-definition-repository'

import { productDefinitionList } from './application/product-definition-list'
import { productDefinitionFilter } from './application/product-definition-filter'
import { productDefinitionDetail } from './application/product-definition-detail'

const repo = new ProductDefinitionRepositoryHttp()

export const ProductDefinitionService = {
  list: productDefinitionList(repo),
  filter: productDefinitionFilter(),
  detail: productDefinitionDetail(repo)
}
