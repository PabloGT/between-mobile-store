import { ProductDefinition } from '../../domain/product'

export const productDefinitionAdapterResponse = function (
  productDefinitionResponse
) {
  const product = new ProductDefinition()
  return { ...product, ...productDefinitionResponse }
}
