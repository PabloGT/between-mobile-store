import axios from 'axios'

import { productDefinitionAdapterResponseBase } from './product-definition-adapter-response-base'
import { productDefinitionAdapterResponse } from './product-definition-adapter-response'

const BASE_URL = 'api/product'

export class ProductDefinitionRepositoryHttp {
  async list () {
    return (
      axios
        .get(`${BASE_URL}`)
        .then((res) =>
          (res.data || []).map(productDefinitionAdapterResponseBase)
        )
    )
  }

  async detail (productId) {
    return (
      axios
        .get(`${BASE_URL}/${productId}`)
        .then((res) => productDefinitionAdapterResponse(res.data))
    )
  }
}
