import { ProductDefinitionBase } from '../../domain/product'

export const productDefinitionAdapterResponseBase = function (
  productDefinitionResponseBase
) {
  const product = new ProductDefinitionBase()
  return { ...product, ...productDefinitionResponseBase }
}
