export class ProductDefinitionBase {
  constructor () {
    this.id = ''
    this.brand = ''
    this.model = ''
    this.price = ''
    this.imgUrl = ''
  }
}

export class ProductDefinition extends ProductDefinitionBase {
  constructor () {
    super()
    this.networkTechnology = ''
    this.networkSpeed = ''
    this.gprs = ''
    this.edge = ''
    this.announced = ''
    this.status = ''
    this.dimentions = ''
    this.weight = ''
    this.sim = ''
    this.displayType = ''
    this.displayResolution = ''
    this.displaySize = ''
    this.os = ''
    this.cpu = ''
    this.chipset = ''
    this.gpu = ''
    this.externalMemory = ''
    this.internalMemory = []
    this.ram = ''
    this.primaryCamera = []
    this.secondaryCmera = []
    this.speaker = ''
    this.audioJack = ''
    this.wlan = []
    this.bluetooth = []
    this.gps = ''
    this.nfc = ''
    this.radio = ''
    this.usb = ''
    this.sensors = []
    this.battery = ''
    this.colors = []
    this.options = {
      colors: [],
      storages: []
    }
  }
}

export class ProductInstance {
  constructor (productDefinitionId, color, storage) {
    this.productDefinitionId = productDefinitionId
    this.color = color
    this.storage = storage
  }
}
