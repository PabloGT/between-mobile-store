export function productDefinitionDetail (repo) {
  return async function (productDefinitionId) {
    return await repo.detail(productDefinitionId)
  }
}
