import { productDefinitionFilter } from './product-definition-filter'

const PRODUCT_DEFINITION_BASE_SAMPLE_1 = {
  id: 'ZmGrkLRPXOTpxsU4jjAcv',
  brand: 'Acer',
  model: 'Iconia Talk S',
  price: '170',
  imgUrl:
    'https://front-test-api.herokuapp.com/images/ZmGrkLRPXOTpxsU4jjAcv.jpg'
}

const PRODUCT_DEFINITION_BASE_SAMPLE_2 = {
  id: 'AasKFs5EGbyAEIKkcHQcF',
  brand: 'alcatel',
  model: 'Flash (2017)',
  price: '',
  imgUrl:
    'https://front-test-api.herokuapp.com/images/AasKFs5EGbyAEIKkcHQcF.jpg'
}

const PRODUCT_DEFINITION_BASE_LIST = [
  PRODUCT_DEFINITION_BASE_SAMPLE_1,
  PRODUCT_DEFINITION_BASE_SAMPLE_2
]

describe('Product Definition List Filter Use Case Test', () => {
  it('Returns all product definitions without filter', () => {
    // Create use Case
    const filterUseCase = productDefinitionFilter()
    // Execution
    const result = filterUseCase(PRODUCT_DEFINITION_BASE_LIST)

    // Expectations
    const expected = PRODUCT_DEFINITION_BASE_LIST

    expect(result).toEqual(expected)
  })

  it('Returns all product definitions without pattern', () => {
    // Create use Case
    const filterUseCase = productDefinitionFilter()

    // Execution
    const filter = { pattern: '' }
    const result = filterUseCase(PRODUCT_DEFINITION_BASE_LIST, filter)

    // Expectations
    const expected = PRODUCT_DEFINITION_BASE_LIST

    expect(result).toEqual(expected)
  })

  it('Filter by brand', () => {
    // Create use Case
    const filterUseCase = productDefinitionFilter()

    // Execution
    const filter = { pattern: 'Ace' }
    const result = filterUseCase(PRODUCT_DEFINITION_BASE_LIST, filter)

    // Expectations
    const expected = [PRODUCT_DEFINITION_BASE_SAMPLE_1]

    expect(result).toEqual(expected)
  })

  it('Filter by brand with UPPERCASE', () => {
    // Create use Case
    const filterUseCase = productDefinitionFilter()

    // Execution
    const filter = { pattern: 'ACE' }
    const result = filterUseCase(PRODUCT_DEFINITION_BASE_LIST, filter)

    // Expectations
    const expected = [PRODUCT_DEFINITION_BASE_SAMPLE_1]

    expect(result).toEqual(expected)
  })

  it('Filter by model', () => {
    // Create use Case
    const filterUseCase = productDefinitionFilter()
    // Execution
    const filter = { pattern: 'lash' }
    const result = filterUseCase(PRODUCT_DEFINITION_BASE_LIST, filter)

    // Expectations
    const expected = [PRODUCT_DEFINITION_BASE_SAMPLE_2]

    expect(result).toEqual(expected)
  })

  it('Filter by model with UPPERCASE', () => {
    // Create use Case
    const filterUseCase = productDefinitionFilter()
    // Execution
    const filter = { pattern: 'LASH' }
    const result = filterUseCase(PRODUCT_DEFINITION_BASE_LIST, filter)

    // Expectations
    const expected = [PRODUCT_DEFINITION_BASE_SAMPLE_2]

    expect(result).toEqual(expected)
  })
})
