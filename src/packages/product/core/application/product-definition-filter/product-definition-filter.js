export function productDefinitionFilter () {
  return function (productDefinitionList, productDefinitionFilter) {
    if (!productDefinitionFilter || !productDefinitionFilter.pattern) { return productDefinitionList }

    return productDefinitionList.filter(
      (product) =>
        product.brand
          .toLowerCase()
          .includes(productDefinitionFilter.pattern.toLowerCase()) ||
        product.model
          .toLowerCase()
          .includes(productDefinitionFilter.pattern.toLowerCase())
    )
  }
}
