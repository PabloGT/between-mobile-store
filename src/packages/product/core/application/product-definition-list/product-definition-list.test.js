import { productDefinitionList } from './product-definition-list'

const PRODUCT_DEFINITION_BASE_SAMPLE_1 = {
  id: 'ZmGrkLRPXOTpxsU4jjAcv',
  brand: 'Acer',
  model: 'Iconia Talk S',
  price: '170',
  imgUrl:
    'https://front-test-api.herokuapp.com/images/ZmGrkLRPXOTpxsU4jjAcv.jpg'
}

const PRODUCT_DEFINITION_BASE_SAMPLE_2 = {
  id: 'AasKFs5EGbyAEIKkcHQcF',
  brand: 'alcatel',
  model: 'Flash (2017)',
  price: '',
  imgUrl:
    'https://front-test-api.herokuapp.com/images/AasKFs5EGbyAEIKkcHQcF.jpg'
}

const PRODUCT_DEFINITION_BASE_LIST = [
  PRODUCT_DEFINITION_BASE_SAMPLE_1,
  PRODUCT_DEFINITION_BASE_SAMPLE_2
]

describe('Product Definition List Use Case Test', () => {
  it('Calls correcty to repo list method', async () => {
    // Repo Mock
    const repositoryMock = {
      list: jest.fn(() => Promise.resolve(PRODUCT_DEFINITION_BASE_LIST))
    }

    // Create use Case
    const listUseCase = productDefinitionList(repositoryMock)

    // Execution
    const ID = 'id'
    const result = await listUseCase(ID)

    // Expectations
    const expected = PRODUCT_DEFINITION_BASE_LIST

    expect(result).toEqual(expected)
    expect(repositoryMock.list).toBeCalledTimes(1)
  })
})
