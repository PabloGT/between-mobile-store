export function productDefinitionList (repo) {
  return async function () {
    return await repo.list()
  }
}
