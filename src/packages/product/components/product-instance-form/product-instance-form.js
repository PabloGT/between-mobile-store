import { useFormik } from 'formik'
import { ProductInstance } from '../../core/domain/product'

import './product-instance-form.css'

export const ProductInstanceFormComponent = ({
  productDefinition,
  onSubmit
}) => {
  const { values, errors, dirty, isValid, handleChange, handleSubmit } =
    useFormik({
      initialValues: { color: '{}', storage: '{}' },
      validate: (values) => {
        const errors = {}
        if (!JSON.parse(values.color).code) {
          errors.color = 'Debe seleccionar el color'
        }
        if (!JSON.parse(values.storage).code) {
          errors.storage = 'Debe seleccionar el almacenamiento'
        }
        return errors
      },
      onSubmit: (values) => {
        if (!onSubmit) return
        const productInstance = new ProductInstance(
          productDefinition.id,
          JSON.parse(values.color),
          JSON.parse(values.storage)
        )
        onSubmit(productInstance)
      }
    })

  const colors = (productDefinition.options || {}).colors || []
  const storages = (productDefinition.options || {}).storages || []

  return (
    <div className="product-instance-form-component">
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="color">Color</label>
          <select
            id="color"
            name="color"
            onChange={handleChange}
            value={values.color.name}
          >
            <option value={'{}'}></option>
            {colors.map((color, index) => (
              <option value={JSON.stringify(color)} key={index}>
                {color.name}
              </option>
            ))}
          </select>
          {errors.color && <div className="input-feedback">{errors.color}</div>}
        </div>
        <div className="form-group">
          <label htmlFor="storage">Almacenamiento</label>
          <select
            id="storage"
            name="storage"
            onChange={handleChange}
            value={values.storage.name}
          >
            <option value={'{}'}></option>
            {storages.map((storage, index) => (
              <option value={JSON.stringify(storage)} key={index}>
                {storage.name}
              </option>
            ))}
          </select>
          {errors.storage && (
            <div className="input-feedback">{errors.storage}</div>
          )}
        </div>

        <button type="submit" disabled={!dirty || !isValid}>
          Añadir a la cesta
        </button>
      </form>
    </div>
  )
}
