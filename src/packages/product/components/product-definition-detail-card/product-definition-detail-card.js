import './product-definition-detail-card.css'

export const ProductDefinitionDetailCardComponent = ({
  productDefinition,
  ActionsComponent
}) => {
  return (
    <div className="product-definition-detail-card">
      <img
        src={productDefinition.imgUrl}
        alt={`${productDefinition.brand} ${productDefinition.model}`}
      />
      <div className="model">{productDefinition.model}</div>
      <div className="brand">{productDefinition.brand}</div>
      <div className="price">
        {productDefinition.price ? `${productDefinition.price} €` : 'Consultar'}
      </div>
      {ActionsComponent && (
        <div className="actions">
          <ActionsComponent productDefinition={productDefinition} />
        </div>
      )}
    </div>
  )
}
