import './product-definition-detail.css'

export const ProductDefinitionDetailComponent = ({
  productDefinition,
  ActionsComponent
}) => {
  return (
    <div className="product-definition-detail-component">
      <div className="image">
        <img
          src={productDefinition.imgUrl}
          alt={`${productDefinition.brand} ${productDefinition.model}`}
        />
      </div>
      <div className="info">
        <div className="description">
          <dl>
            <dt>Marca</dt>
            <dd>{productDefinition.brand}</dd>
          </dl>
          <dl>
            <dt>Modelo</dt>
            <dd>{productDefinition.model}</dd>
          </dl>
          <dl>
            <dt>Precio</dt>
            <dd>{productDefinition.price || 'Consultar'}</dd>
          </dl>
          <dl>
            <dt>CPU</dt>
            <dd>{productDefinition.cpu || 'No Disponible'}</dd>
          </dl>
          <dl>
            <dt>RAM</dt>
            <dd>{productDefinition.ram || 'No Disponible'}</dd>
          </dl>
          <dl>
            <dt>Sistema Operativo</dt>
            <dd>{productDefinition.os || 'No Disponible'}</dd>
          </dl>
          <dl>
            <dt>Resolucion de pantalla</dt>
            <dd>
              {productDefinition.displaySize} (
              {productDefinition.displayResolution})
            </dd>
          </dl>
          <dl>
            <dt>Bateria</dt>
            <dd>{productDefinition.battery || 'No Disponible'}</dd>
          </dl>
          <dl>
            <dt>Camaras</dt>
            <dd>Principal: {productDefinition.primaryCamera || 'No Disponible'}</dd>
            <dd>Secundaria: {productDefinition.secondaryCmera || 'No Disponible'}</dd>
          </dl>
          <dl>
            <dt>Dimensiones</dt>
            <dd>{productDefinition.dimentions || 'No Disponible'}</dd>
          </dl>
          <dl>
            <dt>Peso</dt>
            <dd>{productDefinition.weight || 'No Disponible'}</dd>
          </dl>
        </div>
        {ActionsComponent && (
          <div className="actions">
            <ActionsComponent productDefinition={productDefinition} />
          </div>
        )}
      </div>
    </div>
  )
}
