import { useEffect, useState } from 'react'

import './product-definition-search.css'

export const ProductDefinitionSearchComponent = ({ filter, onFilterChange }) => {
  const [pattern, setPattern] = useState('')

  useEffect(() => setPattern((filter || {}).pattern || ''), [filter])

  const handleChange = (event) => {
    const newFilter = { pattern: event.target.value || '' }
    setPattern(newFilter.pattern)
    if (onFilterChange) onFilterChange(newFilter)
  }

  return (
    <input
      className="product-definition-search"
      placeholder="Buscar por marca o modelo"
      type="text"
      value={pattern}
      onChange={handleChange}
    />
  )
}
