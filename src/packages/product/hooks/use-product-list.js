import { useEffect, useState } from 'react'
import useSwr from 'swr'

import { ProductDefinitionService } from '../core'

export const useProductList = (filter) => {
  const [list, setList] = useState([])

  const { data, error, isValidating } = useSwr(
    'product',
    () => ProductDefinitionService.list(),
    { dedupingInterval: 3600000, revalidateOnFocus: false }
  )

  useEffect(() => {
    const filteredList = ProductDefinitionService.filter(data || [], filter)
    setList(filteredList)
  }, [filter, data])

  return { list, error, loading: isValidating }
}
