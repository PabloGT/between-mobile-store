import useSwr from 'swr'

import { ProductDefinitionService } from '../core'

export const useProductDetail = (productDefinitionId) => {
  const { data, error, isValidating } = useSwr(
    `product/${productDefinitionId}`,
    () => ProductDefinitionService.detail(productDefinitionId),
    { dedupingInterval: 3600000, revalidateOnFocus: false }
  )

  return { productDefinition: data, error, loading: isValidating }
}
