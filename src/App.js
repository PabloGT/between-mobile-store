import { Routes, Route, Outlet } from 'react-router-dom'

import { EcommerceMainPage } from './modules/ecommerce/pages/ecommerce-main'
import { EcommerceListPage } from './modules/ecommerce/pages/ecommerce-list'
import { EcommerceDetailPage } from './modules/ecommerce/pages/ecommerce-detail'

function App () {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<EcommerceMainPage />}>
          <Route index element={<EcommerceListPage />} />
          <Route
            path=":productDefinitionId"
            element={<EcommerceDetailPage />}
          />
        </Route>
      </Routes>

      <Outlet />
    </div>
  )
}

export default App
