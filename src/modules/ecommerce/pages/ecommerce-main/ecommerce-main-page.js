import { Outlet } from 'react-router-dom'

import { Header } from '../../../../packages/common/components/header'

import { CartDetailContainer } from '../../../../packages/cart/containers/cart-detail-container'

import './ecommerce-main-page.css'

export const EcommerceMainPage = () => {
  return (
    <>
      <Header CustomComponent={CartDetailContainer} />
      <div id="eccomerce-main-page-container">
        <Outlet />
      </div>
    </>
  )
}
