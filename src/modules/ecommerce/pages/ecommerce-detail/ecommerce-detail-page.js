import { useParams, useNavigate } from 'react-router-dom'

import { CartProductAddContainer } from '../../../../packages/cart/containers/cart-product-add-container/cart-product.add-container'

import { ProductDefinitionDetailContainer } from '../../../../packages/product/containers/product-definition-detail'

import './ecommerce-detail-page.css'

export const EcommerceDetailPage = () => {
  const { productDefinitionId } = useParams()
  const navigate = useNavigate()

  return (
    <div id="eccomerce-detail-page-container">
      <div className="top-bar">
        <button onClick={() => navigate(-1)}>{'< '}Volver</button>
      </div>
      <div className="detail">
        <ProductDefinitionDetailContainer
          productDefinitionId={productDefinitionId}
          ActionsComponent={CartProductAddContainer}
        />
      </div>
    </div>
  )
}
