import { useState } from 'react'

import { ProductDefinitionSearchComponent } from '../../../../packages/product/components/product-definition-search'
import { ProductDefinitionListContainer } from '../../../../packages/product/containers/product-definition-list'

import { EcommerceProductDetailLink } from '../../components/ecommerce-product-detail-link'

import './ecommerce-list-page.css'

export const EcommerceListPage = () => {
  const [filter, setFilter] = useState({ pattern: '' })

  const handleFilterChange = (filter) => {
    setFilter(filter)
  }

  return (
    <div id="eccomerce-list-page-container">
      <div className="search">
        <ProductDefinitionSearchComponent onFilterChange={handleFilterChange}/>
      </div>
      <div className="list">
        <ProductDefinitionListContainer
          filter={filter}
          ItemActionsComponent={EcommerceProductDetailLink}
        />
      </div>
    </div>
  )
}
