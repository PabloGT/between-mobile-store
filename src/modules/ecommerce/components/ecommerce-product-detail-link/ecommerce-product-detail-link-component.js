import { Link } from 'react-router-dom'

import './ecommerce-product-detail-link-component.css'

export const EcommerceProductDetailLink = ({ productDefinition }) => {
  return (
    <div className="ecommerce-product-detail-link">
      <Link to={`${productDefinition.id}`}>Ver Más</Link>
    </div>
  )
}
